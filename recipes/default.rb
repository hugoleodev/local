#
# Cookbook Name:: local
# Recipe:: default
#
# Copyright (C) 2013 Hugo Leonardo
# 
# All rights reserved - Do Not Redistribute
#

include_recipe "local::php54-ppa"
include_recipe "local::webserver"
include_recipe "local::database"
include_recipe "local::php"
include_recipe "local::webmail"
include_recipe "local::ftp"
include_recipe "local::analyzers"
