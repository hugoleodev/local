#
# Cookbook Name:: local
# Recipe:: php
#
# Copyright (C) 2013 Hugo Leonardo
# 
# All rights reserved - Do Not Redistribute
#

include_recipe "php"
include_recipe "php::module_mysql"
include_recipe "php::module_apc"
include_recipe "php::module_gd"
include_recipe "php::module_sqlite3"