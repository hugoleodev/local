#
# Cookbook Name:: local
# Recipe:: database
#
# Copyright (C) 2013 Hugo Leonardo
# 
# All rights reserved - Do Not Redistribute
#

include_recipe "mysql::server"
include_recipe "database::mysql"

mysql_connection_info = {
  :host     => 'localhost',
  :username => 'root',
  :password => node['mysql']['server_root_password']
}

mysql_database_user 'mdprod' do
  connection mysql_connection_info
  password   'md1981*!&@'
  host       '%'
  action     [:create, :grant]
end

mysql_database_user 'mdquery' do
  connection mysql_connection_info
  password   'md1974m*!&@'
  host       '%'
  privileges [:select]
  action     [:create, :grant]
end