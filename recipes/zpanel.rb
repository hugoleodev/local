#
# Cookbook Name:: local
# Recipe:: zpanel
#
# Copyright (C) 2013 Hugo Leonardo
# 
# All rights reserved - Do Not Redistribute
#

tarball = "master.tar.gz"
tarball_path = "#{Chef::Config[:file_cache_path]}/#{tarball}"

remote_file tarball_path do
  source "https://github.com/bobsta63/zpanelx/archive/master.tar.gz"
  action :create_if_missing
end

execute "tar" do
	action :run
	command "tar -zxvf #{tarball_path}"
	cwd "/opt/"
	user "root"
	group "root"
	not_if { File.exist?(copy_home) }
end

%w{.copy Copy}.each do |dir|
  directory "/home/server/#{dir}" do
    mode 00775
    owner "server"
    group "server"
    action :create
    recursive true
  end
end

# Copy Daemon
template "/etc/init.d/copyconsole" do
	source "copy_daemon.erb"
	owner "root"
	group "root"
	mode "0755"
	variables( 
		:architecture => architecture,
		:user => node[:navcenter][:user][:name],
		:copy_user => node[:navcenter][:copy][:user],
		:copy_pass =>node[:navcenter][:copy][:pass]
	)
end

execute "create a copyconsole service" do
	command "update-rc.d copyconsole defaults"
	action :run
end

execute "executing copy service" do
	command "service copyconsole start"
	action :run
end

package "git" do
	action :install
end