#
# Cookbook Name:: local
# Recipe:: php54-ppa
#
# Copyright (C) 2013 Hugo Leonardo Costa e Silva
# 
# All rights reserved - Do Not Redistribute
#

execute "apt-get update" do
  command "sudo apt-get update"
  action :nothing
end

apt_repository "php54-ppa" do
  uri "http://ppa.launchpad.net/ondrej/php5-oldstable/ubuntu"
  distribution node['lsb']['codename']
  components ["main"]
  keyserver "keyserver.ubuntu.com"
  key "E5267A6C"
  action :add
  notifies :run, "execute[apt-get update]", :immediately
end