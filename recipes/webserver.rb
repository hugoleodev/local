#
# Cookbook Name:: local
# Recipe:: webserver
#
# Copyright (C) 2013 Hugo Leonardo
# 
# All rights reserved - Do Not Redistribute
#

include_recipe "apache2"
include_recipe "apache2::mod_ssl"
