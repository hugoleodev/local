name             'local'
maintainer       'YOUR_NAME'
maintainer_email 'YOUR_EMAIL'
license          'All rights reserved'
description      'Installs/Configures local'
long_description IO.read(File.join(File.dirname(__FILE__), 'README.md'))
version          '0.1.0'

depends "apache2"
depends "postfix"
depends "postfix-dovecot"
depends "mysql"
depends "database"
depends "php"
depends "proftpd"
depends "webalizer"